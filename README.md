# blih-clone-all
Clone all your repos from the epitech git server with this simple bash script.

# Requirements
- blih
- git


# Usage

./blih-clone x.x@epitech.eu


And then watch it clone dozens if not thousands of git repositories! Enjoy.
